#include "ACGKinect2.hh"
#include <iostream>

#ifdef USE_NITE2




// ==================================================================
// User data
// ==================================================================


/** @brief Returns the total number of users in the Kinect's FOV.
 *
 */

int KinectInfo::userCount() {
  if ( !niteInitialized_) {
    std::cerr << "Error, Nite not initialized" << std::endl;
    return 0;
  }

  return userData_.size();
}

//-----------------------------------------------------------------------------------------------------

/** @brief Retrieves the position of a particular joint for a specified user
 *
 */

ACG::Vec3d KinectInfo::getSkeletonJointPosition(const int _userId , const nite::JointType _joint, float* _confidence ) {

  nite::SkeletonJoint thisJoint;
  nite::Point3f pos;

  for (unsigned int i = 0; i < userData_.size(); i++){
    if (userData_[i].getId() == _userId){
      thisJoint = userData_[i].getSkeleton().getJoint(_joint);
      pos = thisJoint.getPosition();

      if ( _confidence)
        *_confidence = thisJoint.getPositionConfidence();
    }
  }

  return ACG::Vec3d(pos.x,pos.y,pos.z);

}

//-----------------------------------------------------------------------------------------------------

/** @brief Retrieves the quaternion of a particular joint for a specified user
 *
 */

ACG::Quaternionf KinectInfo::getSkeletonJointQuaternion(const int _userId , const nite::JointType _joint, float* _confidence ) {

    nite::SkeletonJoint thisJoint;
    nite::Quaternion quaternion;

    for (unsigned int i = 0; i < userData_.size(); i++){
        if (userData_[i].getId() == _userId){
            thisJoint = userData_[i].getSkeleton().getJoint(_joint);
            quaternion = thisJoint.getOrientation();

            if ( _confidence)
              *_confidence = thisJoint.getPositionConfidence();
        }
    }

    return ACG::Quaternionf(quaternion.w,quaternion.x,quaternion.y,quaternion.z);
}

//-----------------------------------------------------------------------------------------------------

/** @brief Specifies if the given user is being tracked or not.
 *
 */

bool KinectInfo::isUserTracked(const int _userId) {

  for (size_t i = 0 ; i < userData_.size(); ++i )
    if ( userData_[i].getId() == _userId ) {
      return (userData_[i].getSkeleton().getState() == nite::SKELETON_TRACKED );
    }

  return false;

}

//----------------------------------------------------------------------------------------------------

/** @brief Retrieves the bounding box of the user skeleton for a given user.
 *
 */

void KinectInfo::getBoundingBox (const int _userId, ACG::Vec3f& _min, ACG::Vec3f& _max ) {

    for ( unsigned int i = 0 ; i < userData_.size(); ++i ) {
        if ( userData_[i].getId() == _userId ){


            const nite::BoundingBox boundingBox = userData_[i].getBoundingBox();

            _min = ACG::Vec3f(boundingBox.min.x,boundingBox.min.y,boundingBox.min.z);
            _max = ACG::Vec3f(boundingBox.max.x,boundingBox.max.y,boundingBox.max.z);

        }
    }

    std::cerr << "Unknown user Id "<< _userId << std::endl;

}

//-----------------------------------------------------------------------------------------------------

/** @brief Retrieves the 3D coordinates of the center of mass of the user.
 *
 */

ACG::Vec3f KinectInfo::getUserCoM(const int _id) {

    for ( unsigned int i = 0 ; i < userData_.size(); ++i ) {
        if ( userData_[i].getId() == _id ){
            const nite::Point3f com = userData_[i].getCenterOfMass();

            return (ACG::Vec3f(com.x,com.y,com.z));
        }
    }

    return (ACG::Vec3f(0.0,0.0,0.0));
}

//-----------------------------------------------------------------------------------------------------

// ==================================================================
// Room data
// ==================================================================

/** @brief Reads the floor plane parameters from the kinect. The parameters read are the origin and the normal.
 *
 */

float KinectInfo::getFloorPlane(ACG::Vec3f& origin, ACG::Vec3f& normal) {

    nite::Plane plane = userFrame_.getFloor();

    const float confidence = userFrame_.getFloorConfidence();

    if (  confidence < 0.5 ) {
        return confidence;
    }

    origin = ACG::Vec3f(plane.point.x,plane.point.y,plane.point.z);
    normal = ACG::Vec3f(plane.normal.x,plane.normal.y,plane.normal.z);

    return confidence;

}

//-----------------------------------------------------------------------------------------------------


/** @brief Read the user map from the Scene Analyzer
 *
 */

QImage* KinectInfo::getUserMap() {

  const unsigned int width  = depthFrame_.getWidth();
  const unsigned int height = depthFrame_.getHeight();

  userMap_->fill(0);

  // In case no user has been tracked, retunrs 0
  if (userData_.empty() ){
    return 0;
  }


  // Getting the information of which user is in each pixel
  const nite::UserMap userMap = userFrame_.getUserMap();
  const nite::UserId* pixelMap = userMap.getPixels();

  for (unsigned int i = 0; i < width; ++i){
    for (unsigned int j = 0; j < height; ++j){
      for (unsigned int k = 0; k < userData_.size(); k++){
        if (pixelMap[j*width+i] == userData_[k].getId()){
          userMap_->setPixel(i, j, colorPool_[k]);
        }
      }
    }
  }

  return userMap_;

}

//-----------------------------------------------------------------------------------------------------

void KinectInfo::callbackNewUserDataAvailable() {

  if ( !initialized_)
    return;

  nite::Status frameStatus;

  frameStatus = userTracker_.readFrame(&userFrame_);

  if (frameStatus != nite::STATUS_OK){
    std::cerr << "Couldn't create user tracker\n" << std::endl;
    return;
  }

  //Get used depth frame to keep sync
  depthFrame_ = userFrame_.getDepthFrame();

  const nite::Array<nite::UserData>& users = userFrame_.getUsers();

  // Create a permanant copy to work with
  userData_.clear();

  for ( int i = 0 ; i < users.getSize() ; ++i) {
    userData_.push_back(users[i]);

    const int userId = users[i].getId();

    // This user is new
    if (users[i].isNew()) {
      std::cerr << "============================================" << std::endl;
      std::cerr << "New User " <<  userId << std::endl;

      // New user, so we should start tracking him
      userTracker_.startSkeletonTracking(userId);
      userTracker_.startPoseDetection(userId, nite::POSE_PSI);

      emit newUserDetected(userId);
    }

    // This user is lost
    if (users[i].isLost()) {
      std::cerr << "User " << userId << " is lost" << std::endl;
      emit userLost(userId);
    }

    //std::cerr << "Current User State:" <<  userId << std::endl;

    switch(users[i].getSkeleton().getState())
    {
      case nite::SKELETON_NONE:
        //std::cerr << "Stopped tracking"<< std::endl;
        break;
      case nite::SKELETON_CALIBRATING:
        //std::cerr << "Calibrating..."<< std::endl;
        break;
      case nite::SKELETON_TRACKED:
        //std::cerr << "Tracking!" << std::endl;
        emit updatedSkeleton(userId);
        break;
      case nite::SKELETON_CALIBRATION_ERROR_NOT_IN_POSE:
      case nite::SKELETON_CALIBRATION_ERROR_HANDS:
      case nite::SKELETON_CALIBRATION_ERROR_LEGS:
      case nite::SKELETON_CALIBRATION_ERROR_HEAD:
      case nite::SKELETON_CALIBRATION_ERROR_TORSO:
        std::cerr << "Calibration Failed... :-|"<< std::endl;
        break;
    }


  }

  emit signalNewUserDataAvailable();
}

//-----------------------------------------------------------------------------------------------------

#endif
