#include "ACGKinect2.hh"
#include <iostream>

static KinectInfo* instance = NULL;

// ==================================================================
// Callback classes
// ==================================================================

class ColorStreamListener: public openni::VideoStream::NewFrameListener {
public:
    ColorStreamListener() { };

    virtual ~ColorStreamListener() {};

    virtual void onNewFrame(openni::VideoStream& _stream)
    {
        if (instance != NULL)
            instance->callbackNewColorDataAvailable();
        else
            std::cerr << "Critical error, no KinectInfo instance to call for new Frame!!!" << std::endl;
    }
};

class DepthStreamListener: public openni::VideoStream::NewFrameListener {
public:
    DepthStreamListener() { };

    virtual ~DepthStreamListener() {};

    virtual void onNewFrame(openni::VideoStream& _stream)
    {
        if (instance != NULL)
            instance->callbackNewDepthDataAvailable();
        else
            std::cerr << "Critical error, no KinectInfo instance to call for new Frame!!!" << std::endl;
    }
};

#ifdef USE_NITE2

// ==================================================================
// Callback classes Nite
// ==================================================================

class UserListener: public nite::UserTracker::NewFrameListener {
public:
    UserListener() { };

    virtual ~UserListener() {};

    virtual void onNewFrame(nite::UserTracker & _tracker)
    {
        if (instance != NULL)
            instance->callbackNewUserDataAvailable();
        else
            std::cerr << "Critical error, no KinectInfo instance to call for new User data!!!" << std::endl;
    }
};

// ==================================================================
// Callback registration
// ==================================================================

void KinectInfo::callbackRegisterUser()
{
    if (userListener_ == NULL)
        userListener_ = new UserListener();

    userTracker_.addNewFrameListener(userListener_);
}

//-----------------------------------------------------------------------------------------------------

void KinectInfo::callbackUnregisterUser()
{
    // Unregister the callback if set
    if (userListener_) {
        userTracker_.removeNewFrameListener(userListener_);
        delete userListener_;
        userListener_ = 0;
    }
}

#endif

// ==================================================================
// Callback registration
// ==================================================================


void KinectInfo::callbackRegisterDepth()
{
    if (depthListener_ == NULL)
        depthListener_ = new DepthStreamListener();

    depthStream_.addNewFrameListener(depthListener_);
}
//-----------------------------------------------------------------------------------------------------

void KinectInfo::callbackRegisterColor()
{
    if (colorListener_ == NULL)
        colorListener_ = new ColorStreamListener();

    colorStream_.addNewFrameListener(colorListener_);
}

//-----------------------------------------------------------------------------------------------------

void KinectInfo::callbackUnregisterDepth()
{
    // Unregister the callback if set
    if (depthListener_) {
        depthStream_.removeNewFrameListener(depthListener_);
        delete depthListener_;
        depthListener_ = 0;
    }
}

//-----------------------------------------------------------------------------------------------------

void KinectInfo::callbackUnregisterColor()
{
    // Unregister the callback if set
    if (colorListener_) {
        colorStream_.removeNewFrameListener(colorListener_);
        delete colorListener_;
        colorListener_ = 0;
    }
}

//-----------------------------------------------------------------------------------------------------

/** @brief Function to shut down Kinect.
 *
 */

void KinectInfo::releaseKinect()
{

  // Set this here to make sure, no function will still access anything
  initialized_ = false;

  // Remove the callback hooks

  callbackUnregisterDepth();
  callbackUnregisterColor();

#ifdef USE_NITE2

  callbackUnregisterUser();
  userFrame_.release();
  userTracker_.destroy();

  // Shutdown nite
  nite::NiTE::shutdown();

  delete userMap_;
  userMap_ = NULL;

  userData_.clear();

#endif

  depthStream_.destroy();
  depthFrame_.release();

  colorStream_.destroy();
  colorFrame_.release();

  device_.close();

  // Shutdown OpenNI
  openni::OpenNI::shutdown();


  delete depthImage_;
  depthImage_ = NULL;

  colorFrameCnt_ = 0;
  depthFrameCnt_ = 0;

  colorPool_.clear();

  lastImageFrame_ = 0;

  lastDepthImageFrame_ = 0;

  unlockDepthMutex();

}

//-----------------------------------------------------------------------------------------------------

/** @brief Function to manually initialize the kinect interface in case the kinect has not been connected when OpenFlipper starts. Facilitates interface initialization at any point during runtime.
 *
 */

bool KinectInfo::initKinect()
{
    std::cout << "Initialize KinectInfo" << std::endl;

    //================================================================================
    // Initialize OpenNI
    //================================================================================
    openni::Status stat = openni::OpenNI::initialize();

    if (stat == openni::STATUS_OK) {
        std::cout << "InitOpenNI2: Ok" << std::endl;
    } else {
        std::cerr << openni::OpenNI::getExtendedError() << std::endl;
        return false;
    }

    //================================================================================
    // Initialize Kinect
    //================================================================================

    const char* deviceURI = openni::ANY_DEVICE;

    stat = device_.open(deviceURI);
    if (stat != openni::STATUS_OK) {
        std::cerr << "Device open failed: " << openni::OpenNI::getExtendedError() << std::endl;
        openni::OpenNI::shutdown();
        return false;
    }

    //================================================================================
    // Initialize RGB Image stream
    //================================================================================

    stat = colorStream_.create(device_, openni::SENSOR_COLOR);
    if (stat == openni::STATUS_OK) {
        stat = colorStream_.start();
        if (stat) {
            std::cerr << "Couldn't start color stream: " << openni::OpenNI::getExtendedError() << std::endl;
            colorStream_.destroy();
            openni::OpenNI::shutdown();
            return false;
        }
    } else {
        std::cerr << "Couldn't find color stream: " << openni::OpenNI::getExtendedError() << std::endl;
    }

    //================================================================================
    // Initialize Depth stream
    //================================================================================

    stat = depthStream_.create(device_, openni::SENSOR_DEPTH);
    if (stat == openni::STATUS_OK) {
        stat = depthStream_.start();
        if (stat != openni::STATUS_OK) {
            std::cerr << "Couldn't start depth stream: " << openni::OpenNI::getExtendedError() << std::endl;
            depthStream_.destroy();
            openni::OpenNI::shutdown();
            return false;
        }
    } else {
        std::cerr << "Couldn't find depth stream: " << openni::OpenNI::getExtendedError() << std::endl;
    }

    //================================================================================
    // Initialize image stream
    //================================================================================

    depthVideoMode_ = depthStream_.getVideoMode();
    colorVideoMode_ = colorStream_.getVideoMode();


//    imageWidth_ = colorVideoMode_.getResolutionX();
//    imageHeight_ = colorVideoMode_.getResolutionY();

    // NOTE: changing definition of imageWidth_ to what the depth says
    imageWidth_ = depthVideoMode_.getResolutionX();
    imageHeight_ = depthVideoMode_.getResolutionY();
    colorWidth_ = colorVideoMode_.getResolutionX();
    colorHeight_ = colorVideoMode_.getResolutionY();

    std::cerr << "OpenNI initialization complete. Depth resolution : " << imageWidth_ << " " << imageHeight_ << std::endl;
    std::cerr << "OpenNI initialization complete. Image resolution : " << colorWidth_ << " " << colorHeight_ << std::endl;


#ifdef USE_NITE2
    nite::Status niteRc = nite::NiTE::initialize();

    if (niteRc != nite::STATUS_OK)
    {
        std::cerr << "Nite initialization failed" << std::endl;
    } else {

        niteRc = userTracker_.create(&device_);

        if (niteRc != nite::STATUS_OK) {
            std::cerr << "Faild to create user tracker" << std::endl;
            return false;
        }

    }

    //================================================================================
    // Initialize user map
    //================================================================================

    userMap_ = new QImage(imageWidth_, imageHeight_, QImage::Format_RGB16);

    // By default, the image is filled with black
    userMap_->fill(0);

    //================================================================================
    // Nite initialized
    //================================================================================

    niteInitialized_ = true;

#endif

    //Setup the color pool
    colorPool_.clear();
    colorPool_.push_back(qRgb(0,0,255));               //Moderate blue #0000FF
    colorPool_.push_back(qRgb(0,255,255));             //Cyan blue #00FFFFF
    colorPool_.push_back(qRgb(204,0,0));               //Red #CC0000
    colorPool_.push_back(qRgb(153,0,204));             //Violet #9900CC
    colorPool_.push_back(qRgb(102,204,51));            //Green #66CC33

    initialized_ = true;
    return true;

}

//----------------------------------------------------------------------------------------------

void KinectInfo::setPOVtoImage(bool _enable)
{
  if ( !initialized_) {
    std::cerr << "Error, Kinect not initialized" << std::endl;
    return;
  }

  if ( device_.isImageRegistrationModeSupported (openni::IMAGE_REGISTRATION_DEPTH_TO_COLOR) ) {

    if (_enable ) {
//        device_.setDepthColorSyncEnabled(_enable);
      device_.setImageRegistrationMode(openni::IMAGE_REGISTRATION_DEPTH_TO_COLOR);
    } else {
      device_.setImageRegistrationMode(openni:: IMAGE_REGISTRATION_OFF);
    }

  } else {
    std::cerr << "Image Registration not supported by this device!" << std::endl;
  }
}

//-----------------------------------------------------------------------------------------------------

void KinectInfo::mirrorColorImage(bool _enable) {
  if ( !initialized_) {
    std::cerr << "Error, Kinect not initialized" << std::endl;
    return;
  }

  colorStream_.setMirroringEnabled(_enable);
}

//-----------------------------------------------------------------------------------------------------

void KinectInfo::mirrorDepthImage(bool _enable) {
  if ( !initialized_) {
    std::cerr << "Error, Kinect not initialized" << std::endl;
    return;
  }

  depthStream_.setMirroringEnabled(_enable);
}

//-----------------------------------------------------------------------------------------------------


void KinectInfo::synchronizeFrames(bool _sync)
{
  if ( !initialized_) {
    std::cerr << "Error, Kinect not initialized" << std::endl;
    return;
  }

  device_.setDepthColorSyncEnabled (_sync);
}



// ==================================================================
// User data
// ==================================================================

//-----------------------------------------------------------------------------------------------------



// ==================================================================
// Room data
// ==================================================================

/** @brief Reads the maximum depth of the environment percieved by the kinect.
 *
 */

double KinectInfo::getMaxDepth() {
  if ( !initialized_) {
    std::cerr << "Error, Kinect not initialized" << std::endl;
    return 1.0;
  }

  return depthStream_.getMaxPixelValue();
}


// ==================================================================
// Image retrieval
// ==================================================================


/** @brief Read the RGB image from the Image Generator of the kinect.
 *
 */

void KinectInfo::getQRGBImage(QImage* _colorImage)
{

    // skip calculation if no new frame is available or image == NULL
    if ((lastImageFrame_ != colorFrame_.getFrameIndex()) && _colorImage) {

        // remember frame
        lastImageFrame_ = colorFrame_.getFrameIndex();

        const unsigned int width  = colorFrame_.getWidth();
        const unsigned int height = colorFrame_.getHeight();

        // assign rgb values to QImage data structure efficiently
        memcpy(_colorImage->bits(), colorFrame_.getData(), height * width * 3 * sizeof(uchar));

    }

}
//-----------------------------------------------------------------------------------------------------

/** @brief Read the depth map from the Depth Generator.
 * Depth values measure the distance from the sensor _along the optical axis_ (z not euclidean depth) in millimeters
 */
QImage* KinectInfo::getQDepthImage8Bit()
{

    // Create our image if it does not exist yet
    if ( !depthImage_)
        depthImage_ = new QImage(imageWidth_,imageHeight_,QImage::Format_RGB888);

    //std::cerr <<"Reading frame " << depthFrame_.getFrameIndex() << std::endl;

    const openni::DepthPixel* dataPointer = static_cast<const openni::DepthPixel*>(depthFrame_.getData());
    const int rowSize = depthFrame_.getStrideInBytes() / sizeof(openni::DepthPixel);

    for(int i = 0 ; i < imageWidth_; i++) {
        for(int j = 0; j < imageHeight_; j++) {

            //Copy the color values of the depth image and place them correctly in the pixel pool pointer - depthImage_
            const unsigned int tmp = *(dataPointer+j*rowSize + i);
            unsigned int const color = abs(double( tmp / 10000.0f * 255.0f ));

            //Giving all three values equal in the qRgb function results in a black and white image
            depthImage_->setPixel(i,j, qRgb(color,color,color)  );
        }
    }

    return depthImage_;

}

//-----------------------------------------------------------------------------------------------------


///** @brief Read the raw color map from the Depth Generator.
// *
// */
//void KinectInfo::getRawColorImage24Bit( std::vector<uchar>* colorimage )
//{
//    // image has to be created by caller
//    assert( colorimage );
//    assert( colorimage->size() != 0 );
//
//    //Return the color image of the camera's feed
//    if ( !colorImageInitialized() ) {
//        std::cerr << "[ERROR]: KinectInfo::getRawColorImage24Bit(): Image generator not yet initialized" << std::endl;
//        exit(0);
//    }
//    else {
//        imageGenerator_.GetMetaData(imageMD_);
//        // Only process if frame has changed
//        if ( lastImageFrame_ != imageMD_.FrameID() ) {
//            // remember the last frame
//            lastImageFrame_ = imageMD_.FrameID();
//
//            const unsigned int width = imageMD_.XRes();
//            const unsigned int height = imageMD_.YRes();
//            //Get the current color map from the kinect
//            const XnRGB24Pixel *colorImagePixels =  imageGenerator_.GetRGB24ImageMap();
//
//            for(unsigned int x( 0 ); x < width; ++x)
//            {
//                for (unsigned int y( 0 ); y < height; ++y)
//                {
//                    XnRGB24Pixel const currentPixel = colorImagePixels[(y * width) + x];
//                     //Copy the color values of the color image and place them correctly
//                    (*colorimage)[ (y*width + x)*3     ] = currentPixel.nRed;
//                    (*colorimage)[ (y*width + x)*3 + 1 ] = currentPixel.nGreen;
//                    (*colorimage)[ (y*width + x)*3 + 2 ] = currentPixel.nBlue;
//                }
//            }
//        }
//        //else printf("\n\nCOLOR IMAGE NOT CHANGED\n\n");
//    }
//}
////-----------------------------------------------------------------------------------------------------

/**
 * @brief Return the raw depth map from the Depth Generator.
 * Depth values measure the distance from the sensor _along the optical axis_ (z not euclidean depth) in millimeters
 */
void KinectInfo::getRawDepthImage16Bit( uint16 depthImage[]){

    //    openni::VideoFrameRef pFrame_;
    //    depthStream_.readFrame(&pFrame_);

    // Skip calculation if no new frame is available
    if (lastDepthImageFrame_ != depthFrame_.getFrameIndex() && depthImage) {

        // remember frame
        lastDepthImageFrame_ = depthFrame_.getFrameIndex();

        const unsigned int width  = depthFrame_.getWidth();
        const unsigned int height = depthFrame_.getHeight();

        //Get the current depth map from the kinect
        const openni::DepthPixel* dataPointer = static_cast<const openni::DepthPixel*>(depthFrame_.getData());

        // Copy the raw information from the depth frame into depthImage[]
        memcpy(depthImage, dataPointer, height * width * sizeof(uint16));

    }
}

////-----------------------------------------------------------------------------------------------------

/**
 * @brief Return the positive raw depth map from the Depth Generator.
 * Depth values measure the distance from the sensor _along the optical axis_ (z not euclidean depth) in millimeters
 */

void KinectInfo::getRawPosDepthImage16Bit( uint16 depthImage[]) {

    //    openni::VideoFrameRef pFrame;
    //    depthStream_.readFrame(&pFrame);

    const uint16 maxDepth = uint16(getMaxDepth());

    // Skip this calculation if no new frame is available or image == NULL
    if ( (lastDepthImageFrame_ != depthFrame_.getFrameIndex()) && depthImage ){

        // Remember frame
        lastDepthImageFrame_ = depthFrame_.getFrameIndex();

        const unsigned int width  = depthFrame_.getWidth();
        const unsigned int height = depthFrame_.getHeight();

        // Get the current depth map from the kinect
        const openni::DepthPixel* dataPointer = static_cast<const openni::DepthPixel*>(depthFrame_.getData());

        for (unsigned int i = 0; i < height*width; ++i){
            depthImage[i] = maxDepth - dataPointer[i];
        }
    }
}

////-----------------------------------------------------------------------------------------------------
//
///**
// * @brief Return the raw depth map from the Depth Generator.
// * Depth values measure the distance from the sensor _along the optical axis_ (z not euclidean depth) in millimeters
// */
//void KinectInfo::getRawDepthImage16Bit( float depthImage[] )
//{
////    std::cout << "KinectInfo::getRawDepthImage16Bit" << std::endl;
//
//    if ( !depthImageInitialized_ ) {
//        std::cerr << "[ERROR]: KinectInfo::getRawDepthImage16Bit(): Depth generator not yet initialized" << std::endl;
//        exit(0);
//    }
//
//    depthGenerator_.GetMetaData(depthMD_);
//
//    // skip calculation if no new frame is available or image == NULL
//    if ( ( lastDepthImageFrame_ != depthMD_.FrameID() ) && depthImage )
//    {
//        // remember frame
//        lastDepthImageFrame_ = depthMD_.FrameID();
//
//        unsigned int const width  = depthMD_.XRes();
//        unsigned int const height = depthMD_.YRes();
//        //Get the current depth map from the kinect
//        XnDepthPixel const * const depthPointer = depthGenerator_.GetDepthMap();
//
//        // copy and cast every value from XnUInt16 to float
//        for(unsigned int i = 0; i < width*height; ++i )
//        {
//            depthImage[i] = depthPointer[i];
//        }
//    }
//}
//
////-----------------------------------------------------------------------------------------------------
//
///**
// * @brief Return the flipped (coord origin at bottom left) positive raw depth map from the Depth Generator.
// * Depth values measure the distance from the sensor _along the optical axis_ (z not euclidean depth) in millimeters
// */
//void KinectInfo::getRawPosFlippedDepthImage16Bit( uint16 depthImage[] )
//{
////    std::cout << "KinectInfo::getRawPosFlippedDepthImage16Bit" << std::endl;
//    if ( !depthImageInitialized_ ) {
//        std::cerr << "[ERROR]: KinectInfo::getRawPosFlippedDepthImage16Bit() Depth generator not yet initialized" << std::endl;
//        exit(0);
//    }
//
//    depthGenerator_.GetMetaData(depthMD_);
//    uint16 const maxDepth = uint16(getMaxDepth());
//
//    // skip calculation if no new frame is available or image == NULL
//    if ( ( lastDepthImageFrame_ != depthMD_.FrameID() ) && depthImage )
//    {
//        // remember frame
//        lastDepthImageFrame_ = depthMD_.FrameID();
//
//        unsigned int const width  = depthMD_.XRes();
//        unsigned int const height = depthMD_.YRes();
//        //Get the current depth map from the kinect
//        XnDepthPixel const * const depthPointer = depthGenerator_.GetDepthMap();
//
//        for( unsigned int y( 0 ); y < height; ++y )
//        {
//            for( unsigned int x( 0 ); x < width; ++x )
//            {
//                depthImage[ y*width + x ] = maxDepth - uint16(depthPointer[ (height-y)*width + x ]);
//            }
//        }
//    }
//}

//-----------------------------------------------------------------------------------------------------

void KinectInfo::convertProjectiveToRealWorldCoords ( uint16 depthImage[], std::vector<ACG::Vec3f> & realWorld ) //  const
{
  // check for initialized variables
  if ( depthImage && realWorld.size() != 0 ){

    openni::CoordinateConverter coorConverter;

    float worldX, worldY, worldZ = 0.0;
    ACG::Vec3f tmp;
    for (int y = 0; y < imageHeight_ ; y++){

      for (int x = 0; x < imageWidth_; x++){
        const int index = y * imageWidth_ + x;
        coorConverter.convertDepthToWorld (depthStream_, x, y, depthImage[index], &worldX, &worldY, &worldZ);
        tmp[0] = worldX;
        tmp[1] = worldY;
        tmp[2] = worldZ;
        realWorld[index]=tmp;
      }
    }
  }
}

void KinectInfo::getPixelFromWorldCoord (float _vx, float _vy, float _vz, int* _x, int* _y, uint16* _z){

    openni::CoordinateConverter coorConverter;

    coorConverter.convertWorldToDepth(depthStream_, _vx, _vy, _vz, _x, _y, _z);

}

void KinectInfo::convertDepthPixToColorCoords ( uint16 depthImage[], int x, int y, ACG::Vec2i & colorPix){

    if ( device_.isImageRegistrationModeSupported (openni::IMAGE_REGISTRATION_DEPTH_TO_COLOR) ) {

        openni::Status sta = device_.setImageRegistrationMode(openni::IMAGE_REGISTRATION_DEPTH_TO_COLOR);
        std::cerr << "Is supported?: " << sta << std::endl;

    }
    int colorX = 1;
    int colorY = 1;
    openni::CoordinateConverter coorConverter;
    const int index = y * imageWidth_ + x;
    openni::Status st = coorConverter.convertDepthToColor(depthStream_, colorStream_, x, y, depthImage[index], &colorX, &colorY);

    std::cerr << "status: " << st << std::endl;

    colorPix[0] = colorX;
    colorPix[1] = colorY;

    // std::cerr << "coord x, y: " << x <<" " << y << " is color " << colorX << " " << colorY << std::endl;

}

//-----------------------------------------------------------------------------------------------------

unsigned int KinectInfo::imageWidth() const {
  if ( !initialized_) {
    std::cerr << "Error, Kinect not initialized" << std::endl;
    return 1;
  }

  return depthFrame_.getWidth();
}

//-----------------------------------------------------------------------------------------------------

unsigned int KinectInfo::imageHeight() const  {
  if ( !initialized_) {
    std::cerr << "Error, Kinect not initialized" << std::endl;
    return 1;
  }

  return depthFrame_.getHeight();
}


// ==================================================================
// Callback functions
// ==================================================================

// Called by the external callback if new color images are available
void KinectInfo::callbackNewColorDataAvailable()
{
  if ( !initialized_)
    return;

  openni::Status status = colorStream_.readFrame(&colorFrame_);

  colorFrameCnt_++;
  emit signalNewColorDataAvailable();
}

//-----------------------------------------------------------------------------------------------------

// Called by the external callback if new depth data is available
void KinectInfo::callbackNewDepthDataAvailable()
{
  if ( !initialized_)
    return;

  openni::Status status = depthStream_.readFrame(&depthFrame_);

  depthFrameCnt_++;

  // the mutex prevents the application to get a new frame when the mutex is still locked, i.e. as long the app is processing the last frame
  if (depthImageGeneratorMutex_.tryLock()) {
    emit signalNewDepthDataAvailable();
  } else {
    std::cerr << "[WARNING]: KinectInfo::callbackNewDepthDataAvailable() ***        IGNORING depth frame "
        << depthFrameCnt_ << "       ***" << std::endl;
  }
}

//-----------------------------------------------------------------------------------------------------

// Is called when the application has finished processing the frame and is ready for the next one
void KinectInfo::unlockDepthMutex()
{

    if (depthImageGeneratorMutex_.tryLock()) {
        std::cerr << "[WARNING]: KinectInfo::unlockDepthMutex        *** TRYING TO UNLOCK UNLOCKED MUTEX *** " << std::endl;
    }

    depthImageGeneratorMutex_.unlock();
}

//-----------------------------------------------------------------------------------------------------

// ==================================================================
// Singleton stuff
// ==================================================================

//-----------------------------------------------------------------------------------------------------

// Initialization
KinectInfo* KinectInfo::kinectSingleton_ = 0;

//-----------------------------------------------------------------------------------------------------

// Private constructor
KinectInfo::KinectInfo() :
    depthImage_(NULL),
    userMap_(NULL),
    colorFrameCnt_(0),
    depthFrameCnt_(0),
    colorListener_(NULL),
    depthListener_(NULL),
#ifdef USE_NITE2
    userListener_(NULL),
#endif
    imageWidth_(0),
    imageHeight_(0),
    lastImageFrame_(0),
    lastDepthImageFrame_(0),
    initialized_(false),
    niteInitialized_(false)
{
}

//-----------------------------------------------------------------------------------------------------

// Private empty destructor
KinectInfo::~KinectInfo()
{
  std::cout << "KinectInfo Destructor" << std::endl;
}

//-----------------------------------------------------------------------------------------------------

// Function to get an instance of the KinectInfo Singleton
KinectInfo* KinectInfo::getInstance()
{
    std::cout << "KinectInfo::getInstance()" << std::endl;
    if (!kinectSingleton_) {
        kinectSingleton_ = new KinectInfo();
        instance = kinectSingleton_;
    }
    return kinectSingleton_;
}

//-----------------------------------------------------------------------------------------------------

// Function to destroy the KinectInfo Singleton
void KinectInfo::destroy()
{
    std::cout << "KinectInfo::destroy(): KinectInfo destroy Instance" << std::endl;
    if (kinectSingleton_)
        delete kinectSingleton_;
    instance = NULL;
    kinectSingleton_ = NULL;
}

//-----------------------------------------------------------------------------------------------------
