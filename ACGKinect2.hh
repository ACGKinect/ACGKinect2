/*===========================================================================*\
*                                                                            *
*   $Revision: 10299 $                                                       *
*   $LastChangedBy: moebius $                                                *
*   $Date: 2010-11-25 08:31:40 +0100 (Thu, 25 Nov 2010) $                     *
*                                                                            *
\*===========================================================================*/

#ifndef ACGKINECT2_HH
#define ACGKINECT2_HH



//==============================================================================
// QT includes
//==============================================================================

#include <QObject>
#include <QImage>
#include <QColor>
#include <QMutex>
#include <QTime>
//==============================================================================
// OpenNI includes
//==============================================================================

#include <OpenNI.h>

#ifdef USE_NITE2
  #include <NiTE.h>
#endif

//==============================================================================
// ACG includes
//==============================================================================
#include<ACG/Math/QuaternionT.hh>

//==============================================================================
// get 16 bit datatype
//==============================================================================
typedef unsigned short uint16;


//==============================================================================
// ACG includes
//==============================================================================

#include <ACG/Math/VectorT.hh>

class ColorStreamListener;
class DepthStreamListener;

class UserListener;

class KinectInfo : public QObject {

    Q_OBJECT

public:
    
    //===========================================================================
    /** @name Initialization Routines
     * @{ */
    //===========================================================================

    /** Initializes the kinect
     *
     *   @returns a boolean indicating whether the kinect has been initialized
     */
    bool initKinect();



    /** Stops the Kinect
     */
    void releaseKinect();

    /** @} */


#ifdef USE_NITE2
    //===========================================================================
    /** @name User Data
  * @{ */
    //===========================================================================

signals:
    /** Signal is emitted if a new user is detected.
    *
    * @param _userId id of the new user
    */
    void newUserDetected(int _userId);


    /** Signal is emitted if a user is lost.
    *
    * @param _userId id of the lost user
    */
    void userLost(int _userId);

    /** Signal is emitted if a new skeleton is available for a user
    *
    * @param _userId id of the updated user
    */
    void updatedSkeleton(int _userId);

public:

    /** Number of users in the Kinect's FOV.
    *
    *   @returns the total number of users(calibrated and uncalibrated) within the kinect's field of view.
    */
    int userCount();


    /** Position of a particular joint of a particular user.
    *
    * You have to check if the user is tracked with isUserTracked.
    *
    * @param _userId  Id of the user to get the skeleton joint position
    * @param _joint   Joint identification ... one of:
    * JOINT_HEAD, JOINT_NECK,
    * JOINT_LEFT_SHOULDER
    * JOINT_RIGHT_SHOULDER
    * JOINT_LEFT_ELBOW
    * JOINT_RIGHT_ELBOW
    * JOINT_LEFT_HAND
    * JOINT_RIGHT_HAND
    * JOINT_TORSO
    * JOINT_LEFT_HIP
    * JOINT_RIGHT_HIP
    * JOINT_LEFT_KNEE
    * JOINT_RIGHT_KNEE
    * JOINT_LEFT_FOOT
    * JOINT_RIGHT_FOOT
    * @param _confidence indicates how sure is about the position data stored about this joint. The value is between 0 and 1, with increasing value indicating increasing confidence.
    * @return Position If something went wrong it will return (0,0,0) .. check with success
    */
    ACG::Vec3d getSkeletonJointPosition(const int _userId , const nite::JointType _joint, float* _confidence );


    /** Quaternion of a particular joint of a particular user.
     *
     * You have to check if the user is tracked with isUserTracked.
     *
     * @param _userId  Id of the user to get the skeleton joint position
     * @param _joint   Joint identification ... one of:
     * JOINT_HEAD, JOINT_NECK,
     * JOINT_LEFT_SHOULDER
     * JOINT_RIGHT_SHOULDER
     * JOINT_LEFT_ELBOW
     * JOINT_RIGHT_ELBOW
     * JOINT_LEFT_HAND
     * JOINT_RIGHT_HAND
     * JOINT_TORSO
     * JOINT_LEFT_HIP
     * JOINT_RIGHT_HIP
     * JOINT_LEFT_KNEE
     * JOINT_RIGHT_KNEE
     * JOINT_LEFT_FOOT
     * JOINT_RIGHT_FOOT
     * @param _confidence indicates how sure is about the position data stored about this joint. The value is between 0 and 1, with increasing value indicating increasing confidence.
     * @return Position If something went wrong it will return (0,0,0) .. check with success
     */
    ACG::Quaternionf getSkeletonJointQuaternion(const int _userId , const nite::JointType _joint, float* _confidence );


    /** Check if a user is tracked
    *
    *   @param _userId Id of the user
    *
    *   @returns boolean value indicating whether the user is tracked or not. "True" - Tracked. "False" - untracked.
    */
    bool isUserTracked(const int _userId);


    /** \brief Bounding box of the user skeleton
    *
    *   @param _userId Id of the user
    *   @param _leftBottomNear The bottom left corner of the nearer face of the bounding box
    *   @param _rightTopFar The top right corner of the far face of the bounding box
    *
    *   @returns the _leftBottomNear and _rightTopFar coordinates of the bounding box of the user's skeleton.
    */
    void getBoundingBox(const int _userId,ACG::Vec3f& _leftBottomNear, ACG::Vec3f& _rightTopFar);



    /** \brief Center of mass of the user
    *
    *   @param _id Id of the user for whom center of mass is to be retreived
    *
    *   @returns the center of mass for the specified user
    */
    ACG::Vec3f getUserCoM(const int _id);

private:

    /// Temporary storage of the available user data
    std::vector<nite::UserData> userData_;

    /** @} */

    //===========================================================================
    /** @name Room Data
  * @{ */
    //===========================================================================

public:

    /** \brief Retrieve floor plane
     *
     * @param origin  Plane origin
     * @param normal  Plane normal
     * @return confidence of the current values
     */
    float getFloorPlane(ACG::Vec3f& origin, ACG::Vec3f& normal);

#endif

    /** Maximum depth of the environment
    *
    *   @returns the maximum depth of the environment that the kinect can perceive
    */
    double getMaxDepth();

    /** @} */

    //===========================================================================
    /** @name Image retrieval
  * @{ */
    //===========================================================================

public:

    /** Get the RGB image from the kinect feed
    *
    *   @returns the RGB image feed from the kinect
    */
    void getQRGBImage( QImage* );

    //    /** Get the raw RGB image from the kinect feed
    //    *
    //    *   @returns the raw RGB image feed from the kinect
    //    */
    //    void getRawColorImage24Bit( std::vector<uchar>* colorimage );

    /** Get the 11 bit reduced to 8 bit depth image from the kinect feed
    *
    *   @returns the 8-bit Depth image feed from the kinect
    */
    QImage* getQDepthImage8Bit();

    /** Get the 11 bit depth image from the kinect feed
    *
    *   @returns the 11 bit Depth image feed from the kinect via the argument
    */
    void getRawDepthImage16Bit( uint16[] );
    //    void getRawDepthImage16Bit( float[] );

    /** Get the 11 bit depth image from the kinect feed with positive distance
    *
    *   @returns the 11 bit Depth image feed from the kinect via the argument
    */
    void getRawPosDepthImage16Bit( uint16[] );


    /** Convert the projective coordinates to real world coordinates
     *
     *   @param the depth map
     *
     *   @returns vector containing the (x, y, 11 bit z) world coordinates in mm
     */
    void convertProjectiveToRealWorldCoords ( uint16 depthImage[], std::vector<ACG::Vec3f> & realWorld );

    //    /** Get the 11 bit depth image from the kinect feed with positive distance and coordinate system origin at bottom left
    //    *
    //    *   @returns the 11 bit Depth image feed from the kinect via the argument
    //    */
    //    void getRawPosFlippedDepthImage16Bit( uint16[] );
    //

    /** Return x and y pixel position and z depth from a 3d world vector
     *
     *   @param the 3d world coordinates
     *
     *   @returns x,y position of the pixel and z depth value
     */
    void getPixelFromWorldCoord (float _vx, float _vy, float _vz, int* _x, int* _y, uint16* _z);


    /** Converts the depth coordinates to the color coordinates
     *
     *   @param the depth map and the position of the pixel to be converted
     *
     *   @returns colorX, colorY, coordinates in the color image
     */
    void convertDepthPixToColorCoords ( uint16 depthImage[], int x, int y, ACG::Vec2i & colorPix);


    #ifdef USE_NITE2
    /** Get the user map for all users from the kinect feed
     *
     *   @returns a video feed that displays only user silhouettes
     */
    QImage* getUserMap();
    #endif

    /** \brief Get image width
     *
     * @return Current image width
     */
    unsigned int imageWidth() const;

    /** \brief Get image height
     *
     * @return Current image height
     */
    unsigned int imageHeight() const;

private:

    //    /** @brief Object holding the RGB image retreived from the kinect feed
    //    *
    //    */
    //    QImage* colorImage_;



    /** @brief Object containing the depth image retrieved from the Kinect feed
     *
     */
    QImage* depthImage_;



    /** @brief Object containing the user map retrieved from Kinect feed.
     *
     */
    QImage* userMap_;

    /** @} */

    //
public:

    /**
     * @brief getColorCnt getter for color frame counter
     * @return color frame counter
     */
    inline unsigned int getColorCnt() const { return colorFrameCnt_; }

    /**
     * @brief getDepthCnt getter for depth frame counter
     * @return depth frame counter
     */
    inline unsigned int getDepthCnt() const { return depthFrameCnt_; }


private:

    /// frame counter for color images
    unsigned int colorFrameCnt_;

    /// frame counter for depth images
    unsigned int depthFrameCnt_;

    //===========================================================================
    /** @name Callback functions
  * @{ */
    //===========================================================================

public:
    /// Registers callback on new color image
    void callbackRegisterColor();

    /// Registers callback on new depth image
    void callbackRegisterDepth();

#ifdef USE_NITE2
    /// Registers callback on new user data
    void callbackRegisterUser();

    /// Unregisters callback on new depth image
    void callbackUnregisterUser();

    /// Called by the external callback if new user data is available
    void callbackNewUserDataAvailable();
#endif


    /// Unregisters callback on new color image
    void callbackUnregisterColor();

    /// Unregisters callback on new depth image
    void callbackUnregisterDepth();

    /// is called if a new Image is generated
    void callbackNewColorDataAvailable();

    /// is called if a new Depth Image is generated
    void callbackNewDepthDataAvailable();

    /**
     * @brief unlockDepthMutex: Is called when the application has finished processing the frame and is ready for the next one
     *
     * To prevent the system to be flooded, new depth images are only processed again,
     * after calling this function.
     */
    void unlockDepthMutex();




signals:

    /// Triggered every time if a new color image is available
    void signalNewColorDataAvailable();

    /// Triggered every time if a new depth image is available
    void signalNewDepthDataAvailable();

#ifdef USE_NITE2
    /// Triggered every time if new user data is available
    void signalNewUserDataAvailable();
#endif

private:
    QMutex depthImageGeneratorMutex_;

    /** @} */


    //===========================================================================
    /** @name Image Transformation and controls
     * @{ */
    //===========================================================================

public:

    /** \brief Reproject Depth image to match color image
     *
     * Transform the view point of the depth generator to the one of the image generator
     * This modifies the images, that the depth pixel corresponds to an image pixel.
     *
     * @param _enable Enable or disable transformation
     */
    void setPOVtoImage(const bool _enable);


    /** \brief Enable or disable internal mirroring of color image
     *
     * @param _enable Enable or disable
     */
    void mirrorColorImage(const bool _enable);

    /** \brief Enable or disable internal mirroring of depth image
     *
     * @param _enable Enable or disable
     */
    void mirrorDepthImage(const bool _enable);

    /** \brief Makes the depth and video updates wait for each other
     *
     * @param _sync Enable or disable synchronizing
     */
    void synchronizeFrames(const bool _sync);

    /** @} */

    //===========================================================================
    /** @name Singleton Constructors and destructors
     * @{ */
    //===========================================================================

public:

    /// Get an instance of the KinectInfo
    static KinectInfo* getInstance();

    static void destroy();

    bool initialized() const { return initialized_; };

    int imageWidth_,imageHeight_;
    int colorWidth_,colorHeight_;

private:

    /// Pointer to the instance
    static KinectInfo* kinectSingleton_;

    /// Private copy constructor
    KinectInfo(const KinectInfo&);

    /// Private copy assignment operator
    const KinectInfo& operator= (const KinectInfo&);

    /// Private constructor
    KinectInfo();

    /// Private destructor
    ~KinectInfo();

    /** @} */

    //===========================================================================
    /** @name Internal state variables
  * @{ */
    //===========================================================================

private:

    // The device we are working on
    openni::Device device_;

    /// Color stream from device
    openni::VideoStream  colorStream_;
    openni::VideoMode    colorVideoMode_;
    ColorStreamListener* colorListener_;

    /// Depth stream from device
    openni::VideoStream  depthStream_;
    openni::VideoMode    depthVideoMode_;
    DepthStreamListener* depthListener_;

#ifdef USE_NITE2
    /// User Tracking
    nite::UserTracker         userTracker_;
    nite::UserTrackerFrameRef userFrame_;
    UserListener*             userListener_;
#endif

    /// Color and depth frames
    openni::VideoFrameRef colorFrame_;
    openni::VideoFrameRef depthFrame_;

//    int imageWidth_,imageHeight_;
//    int colorWidth_,colorHeight_;

    /** @brief Color pool for coloring users in the user map
     *
     */
    std::vector<QRgb> colorPool_;

    /** @brief The Last updated Frame in the buffer
     *
     */
    int lastImageFrame_;

    /** @brief The Last updated Depth Frame in the buffer
     *
     */
    int lastDepthImageFrame_;

    /** \brief Flag if kinect has been initialized
     *
     */
    bool initialized_;

    /** \brief Flag if nite has been initialized
     *
     */
    bool niteInitialized_;

    /** @} */

};


//// prevent lots of castings
//inline KinectInfo::ProductionNode operator|(KinectInfo::ProductionNode a, KinectInfo::ProductionNode b)
//{
//    return static_cast<KinectInfo::ProductionNode>( static_cast<int>(a) | static_cast<int>(b) );
//}

#endif //ACGKINECT2_HH
